
// lib/app.ts
import express = require('express');
import bodyparser = require('body-parser');

import state from './state';
import { createNewScavengerhunt } from './services/scavengerhunt_handler';
import Result from './models/result/result';
import Point from './models/points/point';
import IOError from './models/result/io_error';

const app: express.Application = express();

app.use(bodyparser.json())

app.post('/admin/register', function (req, res) {
  const result: Result = createNewScavengerhunt(req.body);
  res.send(result);
});

app.post('/admin/startScavengerhunt', function (req, res) {
  if(req.body.name === null || req.body.password === null) return { result: null, error: { error_code: 0, reason: "Invalid username or password" } };
  
  for(let scavengerhuntIndex = 0; scavengerhuntIndex < state.length; scavengerhuntIndex++) {
    if(req.body.pincode === state[scavengerhuntIndex].pincode) {
      const success = state[scavengerhuntIndex].startScavengerhunt(req.body.admin_code);

      res.send({ result: success, error: null });
      return;
    }
  }
  res.send({ result: null, error: {error_code: 1, reason: "Unknown pincode"} });
});

app.post('/user/checkPincode', function (req, res) {
  for(let scavengerhuntIndex = 0; scavengerhuntIndex < state.length; scavengerhuntIndex++) {
    if(req.body.pincode === state[scavengerhuntIndex].pincode) {
      res.send({ result: true, error: null });
      return;
    }
  }
  res.send({ result: false, error: null });
});
app.post('/user/register', function (req, res) {
  if(req.body.name === null || req.body.password === null) return { result: null, error: { error_code: 0, reason: "Invalid username or password" } };
  let result: Result = { result: null, error: {error_code: 1, reason: "Unknown pincode"} };
  for(let scavengerhuntIndex = 0; scavengerhuntIndex < state.length; scavengerhuntIndex++) {
    if(req.body.pincode === state[scavengerhuntIndex].pincode) {
      result = state[scavengerhuntIndex].addUserToScavengerhunt(req.body.name, req.body.password);
      break;
    }
  }

  res.send(result);
});
app.post('/user/getStatus', function (req, res) {
  if(req.body.name === null || req.body.password === null) return { result: null, error: { error_code: 0, reason: "Invalid username or password" } };
  let point: Point;
  let error: IOError = null;
  let scavengerhuntIndex = null;
  let newPoint = false;
  let users = [];
  let score = null;
  let userIndex = null;
  for(let i = 0; i < state.length; i++) {
    if(req.body.pincode === state[i].pincode) {
      const result = state[i].getPoint(req.body.name, req.body.password);
     
      for(let tempUserIndex = 0; tempUserIndex < state[i].kickedUsers.length; tempUserIndex++) {
        if(req.body.name == state[i].kickedUsers[tempUserIndex].name) {
          res.send({
            hasStarted: null,
            hasEnded: null,
            point: null,
            newPoint: null,
            time_left: null, 
            error: null,
            score: null,
            title: null,
            users: null,
            kicked: true
          });
          state[i].kickedUsers.splice(tempUserIndex, 1);      

          return;
        }
      }

      for(let tempUserIndex = 0; tempUserIndex < state[i].users.length; tempUserIndex++) {
        if(req.body.name == state[i].users[tempUserIndex].name && req.body.password == state[i].users[tempUserIndex].password) {
          userIndex = tempUserIndex;
          users.push({name: state[i].users[tempUserIndex].name, score: state[i].users[tempUserIndex].score});
        } else {
          users.push({name: state[i].users[tempUserIndex].name, score: state[i].users[tempUserIndex].score});
        }
      }
      point = result.result;
      error = result.error;
      scavengerhuntIndex = i;

      break;
    }
  }
  if(scavengerhuntIndex == null) {
    res.send({
      point: null,
      newPoint: false,
      time_left: null,
      score: score,

      error: {
        error_code: 1,
        reason: "Unknown pincode",
      }
    });
    return;
  }
  if(point == undefined || point == null) {
    if(error.error_code == 10) {
      res.send({
        hasStarted: false,
        hasEnded: false,
        point: null,
        newPoint: false,
        time_left: null, 
        error: null,
        score: score,
        title: state[scavengerhuntIndex].title,
        users: users,
        kicked: false

      });
      return;
    }
    res.send({
      hasStarted: state[scavengerhuntIndex].hasStarted,
      hasEnded: state[scavengerhuntIndex].hasEnded,
      score: score,
      point: null,
      newPoint: true,
      time_left: null, 
      error: error,
      users: null,
      kicked: false,
      joinable: state[scavengerhuntIndex].joinable
    });
    return;
  }

  if(point.isCompleted(req.body.input).result && state[scavengerhuntIndex].hasStarted && !state[scavengerhuntIndex].hasEnded) {
    state[scavengerhuntIndex].setUserToNextPoint(req.body.name, req.body.password).result;
    newPoint = true;
    point = state[scavengerhuntIndex].getPoint(req.body.name, req.body.password).result;
  }
  if(point.hasClassifiedCoordinate()) delete point.coordinate;

  score = state[scavengerhuntIndex].users[userIndex].score;
  users.push({name: state[scavengerhuntIndex].users[userIndex].name, score: score});

  res.send({
    hasStarted: state[scavengerhuntIndex].hasStarted,
    hasEnded: state[scavengerhuntIndex].hasEnded,
    point: point,
    kicked: false,
    users: users,
    title: state[scavengerhuntIndex].title,
    newPoint: newPoint,
    time_left: null, 
    score: score,

    error: error, 
  });
});
app.post('/admin/getStatus', function (req, res) {
  if(req.body.name === null || req.body.password === null) return { result: null, error: { error_code: 0, reason: "Invalid username or password" } };
  let scavengerhuntIndex = null;
  let users = [];

  for(let i = 0; i < state.length; i++) {
    if(req.body.pincode === state[i].pincode && req.body.admin_code === state[i].admin_code) {

      for(let userIndex = 0; userIndex < state[i].users.length; userIndex++) {
        users.push({name: state[i].users[userIndex].name, score: state[i].users[userIndex].score})
      }
      scavengerhuntIndex = i;

      break;
    }
  }
  if(scavengerhuntIndex == null) {
    res.send({
      point: null,
      time_left: null, 
      error: {
        error_code: 11,
        reason: "Unknown pincode or admin code",
      }
    });
    return;
  }
  res.send({
    hasStarted: state[scavengerhuntIndex].hasStarted,
    hasEnded: state[scavengerhuntIndex].hasEnded,
    joinable: state[scavengerhuntIndex].joinable,
    users: users,
    time_left: null, 
    error: null, 
  });
});
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
app.post('/admin/toggleJoinable', function (req, res) {


  for(let i = 0; i < state.length; i++) {
    if(req.body.pincode === state[i].pincode && req.body.admin_code === state[i].admin_code) {
      state[i].toggleJoinable();
      res.send({success: true, error: null});
      return;
    }
  }
  res.send({success: false, error: null});

});
app.post('/admin/kickuser', function (req, res) {
  if(req.body.name === undefined) return { result: null, error: { error_code: 0, reason: "Invalid username or password" } };


  for(let i = 0; i < state.length; i++) {
    if(req.body.pincode === state[i].pincode && req.body.admin_code === state[i].admin_code) {

      res.send(state[i].kickUser(req.body.name));
      return;
    }
  }
  res.send({success: false, error: null});

});