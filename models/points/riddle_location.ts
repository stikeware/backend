import Point from "./point";
import Coordinate from "../coordinate";
import Result from "../result/result";
import { calculateDistance } from "../../utils";
import Score from "./scores";
const Distance = require('geo-distance');

export default class RiddleLocation extends Point {
    title: string;
    id: string;
    
    riddle: string;
    coordinate: Coordinate;
    type: string = "riddle_location";

    constructor(title: string, riddle: string, coordinate: Coordinate, scores: Score[]) {
        super(title, scores);

        this.riddle = riddle;
        this.coordinate = coordinate;
    }
    isClassifiedCoordinate(): boolean {
        return true;
    }
    isCompleted(coordinate: any): Result {
       try {
            if(typeof coordinate.latitude !== "number" || typeof coordinate.longitude !== "number") return { result: null, error: { error_code: 7, reason: "Invalid coordinate" } }
            if(calculateDistance(coordinate, this.coordinate) < Distance('50 m')) return { result: true, error: null};
            return { result: false, error: null};
        } catch(e) {
            console.log(e);
            return { result: null, error: { error_code: 7, reason: "Invalid coordinate" } }
        }
    }
}