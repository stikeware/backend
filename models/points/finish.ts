import Point from "./point";
import Coordinate from "../coordinate";
import Result from "../result/result";
import { calculateDistance } from "../../utils";
const Distance = require('geo-distance');

export default class Finish extends Point {
    title: string;
    type: string = "finish";

    constructor(title: string) {
        super(title, []);
        this.title = title;
    }
    hasClassifiedCoordinate(): boolean {
        return false;
    }

}