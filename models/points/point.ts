import Result from "../result/result";
import Coordinate from "../coordinate";
import Score from "./scores";
import { Time } from "../time";

export default class Point {
    title: string;
    coordinate: Coordinate;
    type: string = "point";
    scores: Score[];

    constructor(title: string, scores: Score[]) {
        this.title = title;
        this.scores = scores;
    }

    getScore(beginDate: number, endTime: number): number {
        let score: Score = {
            maxium: Infinity,
            score: 0
        };
        for(let i = 0; i < this.scores.length; i++) {
            if(this.scores[i].maxium < score.maxium &&  endTime - beginDate < this.scores[i].maxium) {
                score = this.scores[i];
            }
        }
        return score.score;
    }

    hasClassifiedCoordinate(): boolean {
        return false;
    }
    isCompleted(object:any): Result {
        return { result: null, error: { error_code: 8, reason: "Invalid function" } } 
    }
}