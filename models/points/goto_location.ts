import Point from "./point";
import Coordinate from "../coordinate";
import Result from "../result/result";
import { calculateDistance } from "../../utils";
import Score from "./scores";
const Distance = require('geo-distance');

export default class GoToLocation extends Point {
    title: string;
    id: string;
    
    coordinate: Coordinate;
    type: string = "goto_location";

    constructor(title: string, coordinate: Coordinate, scores: Score[]) {
        super(title, scores);

        this.coordinate = coordinate;
    }
    isClassifiedCoordinate(): boolean {
        return false;
    }
    isCompleted(coordinate: any): Result {
       try {
            if(typeof coordinate.latitude !== "number" || typeof coordinate.longitude !== "number") return { result: null, error: { error_code: 7, reason: "Invalid coordinate" } }
            if(calculateDistance(coordinate, this.coordinate) < Distance('50 m')) return { result: true, error: null};
            return { result: false, error: null};
        } catch(e) {
            console.log(e);
            return { result: null, error: { error_code: 7, reason: "Invalid coordinate" } }
        }
    }
}