import IOError from "./io_error";

export default interface Result {
    error: IOError;
    result: any;
}