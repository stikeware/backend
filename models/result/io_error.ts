export default interface IOError {
    error_code: number;
    reason: string; 
}