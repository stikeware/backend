import Point from "./points/point";
import User from "./user";
import IOError from "./result/io_error";
import Result from "./result/result";
import { Time } from "./time";

export default class Scavengerhunt {
    title: string;
    pincode: number;
    points: Point[];
    users: User[];
    kickedUsers: User[] = [];
    admin_code: number;
    hasStarted: boolean;
    hasEnded: boolean;
    startedOn: Date;
    joinable: boolean = true;
    maxium_time: number;

    constructor(title: string, pincode: number, points: Point[], users: User[], admin_code: number, maxium_time: number) {
        this.title = title;
        this.pincode = pincode;
        this.points = points;
        this.users = users;
        this.admin_code = admin_code;
        this.hasStarted = false;
        this.hasEnded = false;
        this.maxium_time = maxium_time;
    }
    toggleJoinable() {
        this.joinable = !this.joinable;
    }
    endScavengerhunt() {
        this.hasEnded = true;
    }
    startScavengerhunt(admin_code: number): boolean {
        if(admin_code !== admin_code) return this.hasStarted;
        this.hasStarted = true;
        this.joinable = false;
        if(this.maxium_time != null) {
            //setTimeout(() => { this.endScavengerhunt();}, this.maxium_time);
            this.startedOn = new Date();
        }
        for(let index = 0; index < this.users.length; index++) {
            this.users[index].startedPointOn = new Date().getTime();
        }

        return this.hasStarted;
    }
    kickUser(name: string): Result  {
        if(name === null) return { result: null, error: { error_code: 0, reason: "Invalid username" } };
        
        for(let index = 0; index < this.users.length ; index++) {
            if(this.users[index].name === name) {
                this.kickedUsers.push(this.users[index]);
                this.users.splice(index, 1);      


                return { result: true, error: null };
            }
        }
        return { result: false, error: {error_code: 13, "reason": "User doesn't exist"} };
    }
    addUserToScavengerhunt(name: string, password: string): Result  {
        if(name === null || password === null) return { result: null, error: { error_code: 0, reason: "Invalid username or password" } };
        if(!this.joinable) return { result: null, error: { error_code: 12, reason: "Scavengerhunt isn't joinable anymopre" } };
        
        for(let index = 0; index < this.users.length ; index++) {
            if(this.users[index].name === name) return { result: null, error: { error_code: 2, reason: "name already exists" } };
        }
        for(let index = 0; index < this.kickedUsers.length ; index++) {
            if(this.kickedUsers[index].name === name) return { result: null, error: { error_code: 2, reason: "name already exists" } };
        }
        console.log(name + " has joined scavengerhunt: " + this.pincode);
        this.users.push({name: name, password: password, atPointIndex: 0, score: 0, startedPointOn: null});   
        return { result: true, error: null };
    }
    getPoint(name: string, password: string): Result {
        if(!this.hasStarted) return { result: null, error: { error_code: 10, reason: "Scavengerhunt hasn't started yet" } };
        if(name === null || password === null) return { result: null, error: { error_code: 0, reason: "Invalid username or password" } };
        
        for(let index = 0; index < this.users.length; index++) {
            if(this.users[index].name === name && this.users[index].password === password) {
                const pointIndex = this.users[index].atPointIndex;         
                const point  = this.points[pointIndex];
                
                return { result: point, error: null };
            }
        }
        return { result: null, error: { error_code: 0, reason: "Invalid name/password" }};
                
    }

    setUserToNextPoint(name: string, password: string): Result {
        if(!this.hasStarted) return { result: null, error: { error_code: 10, reason: "Scavengerhunt hasn't started yet" } };
        if(name === null || password === null) return { result: null, error: { error_code: 0, reason: "Invalid username or password" } };

        for(let index = 0; index < this.users.length; index++) {
            if(this.users[index].name === name && this.users[index].password === password) {
                if(this.users[index].atPointIndex < this.points.length) {
                    this.users[index].score += this.points[this.users[index].atPointIndex].getScore( this.users[index].startedPointOn, new Date().getTime());
                    this.users[index].atPointIndex += 1;  

                    this.users[index].startedPointOn = new Date().getTime();
                    
                    return { result: true, error: null }
                } else {
                    return { result: false, error: { error_code: 6, reason: "User is already finished"} };
                }  
            }
        }
        return { result: null, error: { error_code: 0, reason: "Invalid name or password" }};    
    }
}