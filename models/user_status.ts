import Point from "./points/point";
import IOError from "./result/io_error";
import { Time } from "./time";

export interface UserStatus {
    point: Point,
    error: IOError;
    newPoint: boolean;
    time_left: Time;
}