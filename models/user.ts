export default interface User {
    name: String,
    password: String,
    atPointIndex: number,
    score: number,
    startedPointOn: number,
}