import Result from "../models/result/result"
import { JsonToPoint } from "../utils";
import state from '../state';
import Scavengerhunt from "../models/scavengerhunt";
import Point from "../models/points/point";
import Finish from "../models/points/finish";
import { Time } from "../models/time";
export const createNewScavengerhunt = (object: any): Result => {
    try {
        const title = object.title;
        const maxium_time: number = object.maxium_time;
        const pincode = createPincode();
        const admin_code = Math.floor(Math.random() * (99999 - 10000 + 1)) + 9999;

        let points = [];
        try {
            for(let index = 0; index < object.points.length; index++) {
                const point = JsonToPoint(object.points[index]);

                if(point !== null) points.push(point);
            }
        } catch(e) {
            return { result: null, error: {error_code: 3, reason: "No valid points"} }; 
        }
        
        if(points.length < 1) return { result: null, error: {error_code: 3, reason: "No valid points"} };
        points.push(new Finish("Finished with the scavengerhunt"));

        if(typeof title !== "string" || object.title == "") return { result: null, error: { error_code: 4, reason: "Invalid title"} }; 

        const scavengerhunt = new Scavengerhunt(title, pincode, points, [], admin_code, maxium_time);
        state.push(scavengerhunt);
        
        return {result: scavengerhunt, error: null}
    } catch(e) {
        console.log(e);
        
        return { result: null, error: { error_code: 5, reason: "Unknown error"} }; 
    }
}
function createPincode(): number {
    let pincode = Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000;
    for(let scavengerhuntIndex = 0; scavengerhuntIndex < state.length; scavengerhuntIndex++) {
        if(pincode === state[scavengerhuntIndex].pincode) {
            pincode = createPincode();
            break;
        }
    }
    return pincode;
}
