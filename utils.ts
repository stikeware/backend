import Point from "./models/points/point";
import RiddleLocation from "./models/points/riddle_location";

const Distance = require('geo-distance');

import Coordinate from "./models/coordinate";
import GoToLocation from "./models/points/goto_location";
export const JsonToPoint = (object: any) : Point => {
    try {
       switch (object.type) {
           case "RiddleLocation":
            try {
                if(object.title == "" || object.scores.length == 0 || typeof object.title !== "string" || object.riddle == "" || typeof object.riddle !== "string" || typeof object.coordinate.latitude !== "number" || typeof object.coordinate.longitude !== "number") return null;
                for(let i = 0; i < object.scores.length; i++) {
                    if(object.scores[i].maxium == undefined || object.scores[i].score == undefined) return null;
                }
                return new RiddleLocation(object.title, object.riddle, {latitude: object.coordinate.latitude, longitude: object.coordinate.longitude}, object.scores);
            } catch(e) {
                return null;
            }
        case "goto_location":
        try {
            if(object.title == "" || object.scores.length == 0 || typeof object.title !== "string" || typeof object.coordinate.latitude !== "number" || typeof object.coordinate.longitude !== "number") return null;
            for(let i = 0; i < object.scores.length; i++) {
                if(object.scores[i].maxium == undefined || object.scores[i].score == undefined) return null;
            }
            return new GoToLocation(object.title, {latitude: object.coordinate.latitude, longitude: object.coordinate.longitude}, object.scores);
        } catch(e) {
            return null;
        }
        }
    } catch(e) {
    }
    return null;
}


export const calculateDistance = (coordinate: Coordinate, coordinate2: Coordinate): any => {    
    return Distance.between({lat: coordinate.latitude, lon: coordinate.longitude}, {lat: coordinate2.latitude, lon: coordinate2.longitude});

  }